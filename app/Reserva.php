<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
	public function getFecha()
	{

		Carbon::setLocale('es');
		$fecha = Carbon::parse($this->fechaHoraVisita);
		return $fecha;

	}

		public function getPrecio($numeroAlumnos)
	{

		return $numeroAlumnos*2.8;

	}
}
