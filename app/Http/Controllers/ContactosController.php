<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contacto;
use App\Reserva;
use Illuminate\Support\Facades\Storage;

class ContactosController extends Controller
{ 
	
	public function getTodos(){
		$contactos = Contacto::all();
		return view('contactos.index', array('contactos'=>$contactos));
	}
	public function getVer($id)
	{
		$contacto = Contacto::findOrFail($id);
		return view('contactos.mostrar', array('contacto'=>$contacto));
	}
	public function getCrear(){
		$contactos = Contacto::all();
		return view('reservas.crear', array('contactos'=>$contactos));
	}
	public function postCrear(Request $request){

		$reserva = new Reserva();
		$reserva->contacto_id= $request->contacto_id;
		$reserva->fechaHoraVisita = $request->fechaHoraVisita ;
		$reserva->observaciones = $request->observaciones ;
		$reserva->alumnos=$request->alumnos ;
		$reserva->save();
		return redirect('contactos/ver/'.$request->contacto_id);
		
		try{
			$reserva->save();
			return redirect('contactos')->with('mensaje','Reserva: '. $reserva->nombre .' guardada');	
		}catch(\Illuminate\Database\QueryException $ex){
			return redirect('contactos')->with('mensaje','Fallo al crear Reserva');
		}

	}
	public function getEditar($id)
	{
		$reserva = Reserva::findOrFail($id);
		return view('reservas.editar',array('reserva'=>$reserva));
	}
	public function postEditar(Request $request,$id)
	{
		$reserva = Reserva::findOrFail($id);
		$reserva->fechaHoraVisita = $request->fechaHoraVisita ;
		$reserva->observaciones = $request->observaciones ;
		$reserva->alumnos=$request->alumnos ;
		$reserva->save();
		return redirect('contactos');

	}

	public function getExito(){

		return view('contactos.exito');
	}


}
