
@extends('layouts.master')
@section('titulo')
Crear Reserva
@endsection
@section('contenido')
<div class="row">
	<div class="offset-md-3 col-md-6">
		<div class="card">
			<div class="card-header text-center">
				Crear Reserva
			</div>
			<div class="card-body" style="padding:30px">
				{{-- TODO: Abrir el formulario e indicar el método POST --}}
				{{-- TODO: Protección contra CSRF --}}
				<form action="{{ action('ContactosController@postCrear') }}" method="POST" enctype="multipart/form-data">
					
					{{ csrf_field() }}
					<div class="form-group">
						<select name="contacto_id">
							@foreach ($contactos as $contacto)
							<option value="{{$contacto->id}}">{{$contacto->nombre}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="fechaHoraVisita">Fecha Y Hora</label>
						<input type="datetime-local" name="fechaHoraVisita" id="fechaHoraVisita" class="form-control">
					</div>
					<div class="form-group">
						<label for="observaciones">observaciones</label>
						<input type="text" name="observaciones" id="observaciones" class="form-control" >
					</div>
					<div class="form-group">
						<label for="alumnos">Alumnos</label>
						<input type="number" name="alumnos" id="alumnos" class="form-control">
					</div>
					
					<div class="form-group text-center">
						<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
							CrearReserva
						</button>
					</div>
					{{-- TODO: Cerrar formulario --}}
				</form>
			</div>
		</div>
	</div>
</div>
@endsection























