@extends('layouts.master')
@section('titulo')
Editar Reserva
@endsection
@section('contenido')
<div class="row">
	<div class="offset-md-3 col-md-6">
		<div class="card">
			<div class="card-header text-center">
				Editar Reserva
			</div>
			<div class="card-body" style="padding:30px">
				{{-- TODO: Abrir el formulario e indicar el método POST --}}
				{{-- TODO: Protección contra CSRF --}}
				<form action="{{ url('reservas/editar') }}/{{$reserva->id}}" method="POST" enctype="multipart/form-data">
					
						{{ csrf_field() }}
						
						<div class="form-group">
							<label for="fechaHoraVisita">Fecha Y Hora</label>
							<input type="datetime" name="fechaHoraVisita" id="fechaHoraVisita" class="form-control" value="{{$reserva->fechaHoraVisita}}">
						</div>
							<div class="form-group">
							<label for="observaciones">observaciones</label>
							<input type="text" name="observaciones" id="observaciones" class="form-control" value="{{$reserva->observaciones}}">
						</div>
							<div class="form-group">
							<label for="alumnos">Alumnos</label>
							<input type="number" name="alumnos" id="alumnos" class="form-control" value="{{$reserva->alumnos}}">
						</div>
						
						<div class="form-group text-center">
							<button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
								EditarReserva
							</button>
						</div>
						{{-- TODO: Cerrar formulario --}}
					</form>
				</div>
			</div>
		</div>
	</div>
	@endsection
