@extends('layouts.master')
@section('titulo')
Listado de Contactos
@endsection
@section('contenido')
<h4>Todos los contactos</h4>
@if (session('mensaje'))
<div class="alert alert-danger">
	{{session('mensaje')}}
</div>
@endif
<div class="row">
	<div class="card-deck">
		@foreach( $contactos as $contacto )
		<div class="col-xs-12 col-sm-6 col-md-4 ">
			<div class="card" style="width: 20rem;">
				<div class="card-body">
					<h5 class="card-title">{{$contacto->nombre}}</h5>
					<h6 class="card-subtitle mb-2 text-muted">{{$contacto->direccion}}</h6>
					<p class="card-text">{{$contacto->numeroCuenta}}</p>
					<a href="{{ url('/contactos/ver/') }}/{{$contacto->id}}" class="card-link">VER {{$contacto->reservas->count()}}

						@if ($contacto->reservas->count()==1)
						reserva
						@else
						reservas
						@endif

					</a>

					<a href="{{ url('/contactos/exito') }}" class="card-link">Confirmar reservas</a>
				</div>
			</div>
		</div>
	</div>
	@endforeach
</div>
@endsection



