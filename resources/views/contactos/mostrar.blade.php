@extends('layouts.master')
@section('titulo')
Reservas {{$contacto->nombre}}
@endsection
@section('contenido')

<div class="row">
	<div class="col-sm-9">
		<h1>Reservas {{$contacto->nombre}}</h1>
		
		@foreach ($contacto->reservas as $reserva)
		<p class="card-text"><strong>Reservas: </strong><br>

			{{ $reserva->getFecha() }} <br>
			Numero alumnos: {{ $reserva->alumnos }} <br>
			Precio: {{$reserva->getPrecio($reserva->alumnos)}} <br>
			Observaciones: {{$reserva->observaciones}}

		</p>
		<a class="btn btn-warning" href="{{ url('/reservas/editar') }}/{{$reserva->id}}" role="button"> Editar</a>
		<a class="btn btn-light" href="{{ url('/contactos/exito') }}" role="button">Confirmar Reserva</a>

		@endforeach
		
		

	</div>

	
	
</div>
@endsection
