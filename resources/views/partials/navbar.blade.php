<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <img  src="{{asset('assets/imagenes')}}/LOGO.jpg">

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      {{--@if(Auth::check() )--}}
      
      <li class="nav-item">
        <a href="{{url('contactos')}}" class="nav-link {{ Request::is('contactos*') && !Request::is('contactos/crear')? ' active' : ''}}">Listado de Contactos</a>
      </li>
      <li class="nav-item">
        <a href="{{url('/contactos/crear')}}" class="nav-link {{ Request::is('contactos/crear')? ' active' : ''}}">Nueva reserva</a>
      </li>
    </ul>
    {{--@endif --}}
   
  
  </div>
</nav>






