<!doctype html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="stylesheet" type="text/css" href="{{ url('/assets/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('/assets/css/estilo.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}">
	

	<title>	@yield('titulo')</title>
</head>
<body>
	
	@include('partials.navbar')

	
	<div class="container-fluid">
		@yield('contenido')
	</div>
</body>
</html>