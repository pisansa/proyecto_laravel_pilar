<?php

use Illuminate\Database\Seeder;
use App\Contacto;
use App\Reserva;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    private function seedUsuarios(){
    	DB::table('users')->delete();
    	
    	$u = new User();
    	$u->name = "Pilar";
    	$u->email = "pisansa@gmail.com";
    	$u->password = bcrypt("kk");;
    	$u->save();

    	$u = new User();
    	$u->name = "Admin";
    	$u->email = "admin@gmail.com";
    	$u->password = bcrypt("kk");;
    	$u->save();
    	
    }

    private function seedContactos(){
    	DB::table('contactos')->delete();
    	$c = new Contacto();
    	$c->nombre="Colegio Santa Teresa";
    	$c->numeroCuenta="ES34589658248";
    	$c->telefono="942756488";
    	$c->direccion="barrio santa maria de cayon";
    	$c->save();
        $c1 = new Contacto();
        $c1->nombre="Colegio Nuestra señora";
        $c1->numeroCuenta="ES3456456465";
        $c1->telefono="652034697";
        $c1->direccion="ciguenza novales";
        $c1->save();
    }

    private function seedReservas(){
    	DB::table('reservas')->delete();
    	$r = new Reserva();
    	$nc=Contacto::all()->first();
    	$r->contacto_id =$nc->id;
    	$r->alumnos = 25;
    	$r->fechaHoraVisita = '2020-04-16 17:30:00';
    	$r->observaciones ='El grupo variara con la compañia de 3-9 profesores';
    	$r->save();
    	$r1 = new Reserva();
    	$r1->contacto_id =$nc->id;
    	$r1->alumnos = 33;
    	$r1->fechaHoraVisita = '2020-04-17 15:30:00';
    	$r1->observaciones ='El grupo se divide en dos subgrupos';
    	$r1->save();
        $r2 = new Reserva();
        $n2=Contacto::find(3);
        $r2->contacto_id =$n2->id;
        $r2->alumnos = 33;
        $r2->fechaHoraVisita = '2021-03-15 15:30:00';
        $r2->observaciones ='Grupo ingles';
        $r2->save();
    	
   
}
public function run()
{
	$this->seedUsuarios();
	$this->seedContactos();
	$this->seedReservas();
	$this->command->info('Tabla contactos inicializada con datos');

}

}
