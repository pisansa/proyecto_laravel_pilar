<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 
Route::get('/', 'InicioController@getInicio');
Route::get('contactos', 'ContactosController@getTodos');
Route::get('contactos/ver/{id}', 'ContactosController@getVer')->where ('id', '[0-9]+');


Route::get('contactos/crear', 'ContactosController@getCrear');
Route::get('reservas/editar/{id}', 'ContactosController@getEditar')->where ('id', '[0-9]+');

Route::post('contactos/crear', 'ContactosController@postCrear');
Route::post('reservas/editar/{id}', 'ContactosController@postEditar')->where ('id', '[0-9]+');
Route::get('contactos/exito', 'ContactosController@getExito');